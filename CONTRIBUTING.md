# Contribution Guidelines

Thank you for your interest in contributing to the development of Rig!

We largely follow Oracles / the OpenJDK style, both in Java and Xtend files. Please do the same.  
For other files, you should treat it as "When in Rome, do as the romans do" and look at similar files in the repository first.

In general, prefer tabs for indentation, and spaces for alignment.  
Lea Verou has an excellent [write-up on the topic](https://lea.verou.me/2012/01/why-tabs-are-clearly-superior/).

The language used for this project (code, comments, issues, commit messages) is *english*.
