#!/bin/bash
cd info.scce.rig.libraries/
mvn validate -f pom-libraries.xml
cd ..
export RIG_DIR=$PWD
cd /cinco
./cinco-headless.sh $RIG_DIR info.scce.rig PipelineEditor.cpd