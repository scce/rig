---
home: true
title: Low-Code CI/CD
metaTitle: Rig | Low-Code CI/CD Modeling
heroImage: assets/hero-image.png
tagline: Visual Authoring of CI/CD Workflows
actionText: Quick Start →
actionLink: /manual/
features:
- title: Visual Authoring
  details: Use Drag & Drop to compose your CI/CD workflow. Connect components in an intuitive way.
- title: Automated Checks
  details: Rig supports a variety of sanity checks which are constantly evolved in order to make sure CI/CD workflows modeled in Rig are valid and execute as expected.
- title: Code Generation
  details: Rig automatically derives the correct configuration files from the given workflow model.
footer: Code it, Rig it, Ship it continuosly!
---