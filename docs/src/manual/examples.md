# Examples

In this section, you can find example workflow for common use cases.

# Build & Review Documentation: VuePress and Deployment to Review Environments

In this example, we are dissecting how you can build your documentation and not
only deploy one version of it to GitLab pages, but also make all versions of
your documentation available as browsable artifact and environment in which it
can be safely reviewed before merging into your main branch.

The associated workflow -- in this case, from Rig itself -- might look like this:

![Documentation Workflow of Rig](/assets/examples/Pipeline.png)

The goal of the workflow is to allow reviewers to see a live version of the changes
to the documentation before it is merged into main, where it is automatically deployed
to GitLab pages.

Therefore, the build job can be parameterized with the base URL. When building
for the *Production* target, where the artifact is deployed to `scce.gitlab.io/rig`,
the base URL must be set to `/rig/`.

When building for review, where the deployment is done as browsable artifact,
the base URL must be set to
`-/rig/-/jobs/$CI_JOB_ID/artifacts/docs/src/.vuepress/dist/`.

Using a regular `.gitlab-ci.yml` file, this would require either duplication of
the job and using slightly different parameters, possibly leading to inconsistencies
down the line, or making a job template and instantiating it multiple times.

Using Rig, we can simply define one job that takes a `BASE_URL` parameter and
two targets which parameterize the job differently.

Similarly, we can use the *Except* and *Only* elements to add further restrictions
to job execution based on targets.

This has the added advantage of being very consistent. When writing a `.gitlab-ci.yaml`
by hand, we would have to take great care that the `pages` job and `build docs@production` job
have the same conditions and would have to textually duplicate them and apply to both jobs.

Note that you cannot extend from multiple job templates at once in YAML, so this
would not be a solution.

Using Rig, we can simply apply the additional restrictions to targets, and let Rig
auto-derive and emit the proper configuration.

You can see the environments in action in this image, where a deployment has been
made for merge request `!9`:

![Environment for !9](/assets/examples/Pipeline-Environments.png)

Clicking the link will open the version of the documentation as it appears in this
merge request. Reviewers can verify the changes. Upon merging into the main branch, 
the deployment is deleted and the new version is deployed from main.
