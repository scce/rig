# Parameterization

Rig offers three way to parameterize workflow elements, namely

* local values
* variables
* and target parameters.

## Local (literal) values

Local values are the literal values you set directly in the *CINCO Properties* panel.
They can be overriden by variables or target parameters, which take precedence.

A property can *only* be parameterized by a variable *or* target parameter,
never both at the same time.

If an external value (variable or parameter) is connected to a property,
the *padlock* symbol appears, indicating that the element connected
to the property will provide the value and overwrite the locally defined one.

## Variables

In the <kbd>Parameterization</kbd> section of the palette you will find various
types of variables. These variables can be dragged and dropped onto the canvas
and can be connected to any property. They will then supply the value for this
property.

The same variable can be connected to an arbitrary number of properties. This
allows setting the same value consistently in the workflow and having only a
single occurence where it needs to be changed. This can be very helpful e.g. for
version numbers of dependencies, like docker image tags.

## Target Parameters

Targets can have parameters. Parameters work mostly like variables, except that
they only work when their target is triggered during the workflow. This allows
the same job to be run twice (or any number of times), each time with a different
set of parameters that provide the values for the job, e.g. its script argument.

Just like variables, parameters can not only paremeterize jobs, but any property.

## Parameterizable Elements

All properties of workflow elements can be parameterized in these three ways. You
can supply paths for *Artifacts* or *Caches*, you can set branches for *conditions
and much more.

The workflow elements and their properties are described in the next chapter.