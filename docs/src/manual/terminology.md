# Terminology

In this section, we briefly discuss some key terms and how we use them in this manual.

## Workflow

A workflow is the whole, abstract description of our CI/CD process.

## Configuration

Workflows are stored in either one or multiple configuration files, depending on the target platform.
For GitLab, this is the `.gitlab-ci.yml` file.

## Pipeline

A pipeline is a concrete instance of a workflow, with an execution context and tangible results.


For example, the workflow description might include what happens *if* a new commit to a certain branch is made,
the pipeline will be the concrete execution of the workflow when that commit actually occurs. it thus carries context like branch name, date and other contextual information. Thus, not all jobs defined in the workflow
might get executed, but only those whose preconditions are met. This execution usually yields a tangible outcome, like an assembled application and possible the deployment of that application to an environment.