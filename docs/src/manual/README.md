# Introduction

<center><img :src="$withBase('/assets/rig-palette-framed.png')" alt="Workflow Elements used by Rig"></center>

Rig is an Integrated Modeling Environment (IME) for CI/CD Pipelines, and
provides a visual authoring canvas to graphically compose and implement CI/CD
workflows.

Our IME comes with a fully functional code generator and can generate the proper
YAML configuration files from the given pipeline model.

This low-code approach to DevOps aims at closing the semantic gap between the
development and operation teams and to also serve a dual purpose by providing
a clear graphical documentation of the CI/CD pipeline.

We *currently* only support GitLab ^[version 14.2 as of 2021-09-02] as target platform.
