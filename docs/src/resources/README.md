# Resources

The following resources for Rig are currently available:

## Tutorials

* Tegeler T., Teumert S., Schürmann J., Bainczyk A., Busch D., Steffen B. (2021) An Introduction to Graphical Modeling of CI/CD Workflows with Rig. In: Margaria T., Steffen B. (eds) Leveraging Applications of Formal Methods, Verification and Validation. ISoLA 2021. Lecture Notes in Computer Science, vol 13036. Springer, Cham. https://doi.org/10.1007/978-3-030-89159-6_1

## Concept Papers

* Teumert, S.: Visual Authoring of CI/CD Pipeline Configurations. Bachelor’s thesis, TU Dortmund University (2021) ([Web](https://archive.org/details/visual-authoring-of-cicd-pipeline-configurations))
* Tegeler, T., F. Gossen and B. Steffen: A Model-driven Approach to Continuous Practices for Modern Cloud-based Web Applications. In 2019 9th International Conference on Cloud Computing, Data Science Engineering (Confluence), pages 1–6, 2019 ([Web](https://ieeexplore.ieee.org/abstract/document/8776962))

