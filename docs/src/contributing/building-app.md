# Building the Application

You can either work directly with the source and set up Rig inside Eclipse/CINCO,
or make a binary build and work with the generated artifact.

## Generating a binary build (locally)

### Requirements

* **Docker Runtime** (tested on 4.7+)

- Run  `docker run --entrypoint "/bin/bash" --rm -it -v ${PWD}:/build/scce/rig -v "${HOME}/.m2:/root/.m2" --name cinco registry.gitlab.com/scce/cinco`
- Inside the now open container, run
    - `cd /build/scce/rig`
    - `sh build.sh`
- Wait for the build to finish. The first time around, this can take a significant amount of time to download the dependencies. Subsequent runs are faster, because the maven dependencies and image layers are cached on the host.

## Work with the source 

### Requirements

* **[CINCO 2.1.0-SNAPSHOT](https://gitlab.com/scce/cinco)**  
	Nightly builds of CINCO can be found at
	[https://ls5download.cs.tu-dortmund.de/cinco/nightly/](https://ls5download.cs.tu-dortmund.de/cinco/nightly/).
* **Java 11** (Any JDK)

````
git clone git@gitlab.com:scce/rig.git
````

### Install required bundles (for versions <=2575cd7c):

Simply cloning the project and importing it in CINCO will yield many errors due
to missing imports & bundles.  
The required bundles can be found in [https://gitlab.com/scce/p2-maven](https://gitlab.com/scce/p2-maven).

In CINCO, go to <kbd>Help > Install new software</kbd>

* In the *Work with:* field, type `https://scce.gitlab.io/p2-maven/main/repository`
* Install the bundles required for Rig, as shown below:
	![Dependencies](/assets/eclipse-p2-dependencies.png)
	* Eclipse might give you the following warning, which can safely be ignored:
		![Warning](/assets/eclipse-warning.png)  
		(Choose *"Install anyway*)
* Restart CINCO

### Build Rig from source:

* Import the projects into your CINCO workspace.
* In `info.scce.rig.dsl.variables`, find
	`src/info.scce.rig.dsl.variables/GenerateVarExpressions.mwe2`
	and choose <kbd>Run As > MWE2 Workflow</kbd> from the context menu.
* In `info.scce.rig.dsl.conditions`, find
	`src/info.scce.rig.dsl.conditions/GenerateExceptOnly.mwe2`
	and choose <kbd>Run As > MWE2 Workflow</kbd> from the context menu.
* **(for versions >= 2575cd7c)**
	- Run `mvn validate -f pom-libraries.xml` in `info.scce.rig.libraries/`
	- Run `mvn install` in `info.scce.rig.libraries/`
	- Right-Click the Rig Project, Choose "Maven" > "Update Project"
* In `info.scce.rig`, find `model/PipelineEditor.cpd` and chose
	<kbd>Generate Cinco Product</kbd> from the context menu.

**How to Run:**

Right click any of the projects, and select <kbd>Run As > Eclipse Application</kbd>.

### Known Issues

Sometimes, GraText will fail to build due to not importing XTend libraries.

In this case, open  
`info.scce.rig.gratext.ui/src/info/scce/rig/gratext/ui/PipelineGratextProposalProvider.xtend`  
and use the *Quick Fix* "Add Xtend libraries to classpath", then re-generate the CPD.