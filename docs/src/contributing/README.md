# Overview

**Thanks for your interest in contributing to the development of Rig!**

This section contains resources for contributors and developers.

## Repository

Rig is publicly available in our source code repository at
**[https://gitlab.com/Rig](https://gitlab.com/Rig)**.

### Clone via SSH

````
git clone git@gitlab.com:scce/rig.git
````

### Clone via HTTPS

````
git clone https://gitlab.com/scce/rig.git
````


## Project Structure

Rig is a [CINCO Metamodelling Suite](https://cinco.scce.info/) Product.
Currently, CINCO Products are Eclipse Rich Client (RCP) Applications, and thus
run on top of Eclipse. We plan to support more IDEs in the future.

````
.
├── README.md
├── docs                                    // (1) documentation
├── info.scce.rig                           // (2) the main project
├── info.scce.rig.dsl.conditions            // (3) conditions language
├── info.scce.rig.dsl.conditions.ide
├── info.scce.rig.dsl.conditions.tests
├── info.scce.rig.dsl.conditions.ui
├── info.scce.rig.dsl.conditions.ui.tests
├── info.scce.rig.dsl.variables             // (4) variables language
├── info.scce.rig.dsl.variables.ide
├── info.scce.rig.dsl.variables.tests
├── info.scce.rig.dsl.variables.ui
├── info.scce.rig.dsl.variables.ui.tests
├── info.scce.rig.editor.graphiti
├── info.scce.rig.feature
├── info.scce.rig.libraries                 // (5) dependencies
├── info.scce.rig.mcam.views
├── info.scce.rig.pipeline.gratext
├── info.scce.rig.pipeline.gratext.ui
├── info.scce.rig.product
└── workspace
````

The CINCO Product Definition (CPD) for Rig is located in the `info.scce.rig`
folder (2), which is the main project for Rig. The other projects are supporting
projects.
Please refer to [Building the Application](building-app) on how to set up Eclipse
and build Rig.

The `docs` folder (1) contains the source code for this documentation, written
in Markdown and converted to a static webpage via VuePress and deployed to GitLab
Pages.
Please refer to [Building Documentation](building-docs) on how to set up and build
the page.

We release snapshots versions from every commit to
[https://ls5download.cs.tu-dortmund.de/rig/nightly/].

New versions are released from a release branch when tagged with the appropriate 
version tag, and can be found under [https://ls5download.cs.tu-dortmund.de/rig/release/].