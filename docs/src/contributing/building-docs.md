# Building the Documentation

Our documentation is written using [VuePress](https://vuepress.vuejs.org/).

## Requirements

Node.js 14.16.0

## Setup

````
git clone git@gitlab.com:scce/rig.git
pushd rig/docs
npm ci
````

We have included two scripts in order to work with the documentation.

### Development build

````
npm run dev
````

Runs a `webpack-dev-server` and deploys the site to `https://localhost:8080`.

### Production Build

````
npm run build
````

Builds the page to `docs\src\.vuepress\dist` and sets the `base` url to `/rig` for
deployment to GitLab pages ([https://scce.gitlab.io/rig](https://scce.gitlab.io/rig)).