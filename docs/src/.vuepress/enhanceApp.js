/**
 * Client app enhancement file.
 *
 * https://v1.vuepress.vuejs.org/guide/basic-config.html#app-level-enhancements
 */

/*
 * If we ever need to redirect routes, we can do that here, cf.
 * https://github.com/vuejs/vuepress/issues/239
 *
 * router.addRoutes([
 *   { path: '/foo/', redirect: '/' },
 *   { path: '/bar/', redirect: '/' }
 * ])
 */

export default ({
  Vue, // the version of Vue being used in the VuePress app
  options, // the options for the root Vue instance
  router, // the router instance for the app
  siteData // site metadata
}) => {
  // ...apply enhancements for the site.
}
