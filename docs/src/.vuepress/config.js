const { description } = require('../../package')

/*
 * TODO: Add support for prefers-color-scheme:
 * https://tolking.github.io/vuepress-theme-default-prefers-color-scheme/#options
 */

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Rig',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /* Set correct base URL depending on environment var set by script
   * cf. https://github.com/vuejs/vuepress/issues/560
   */
  base: (process.env.BASE_URL == undefined ? '/' : process.env.BASE_URL),

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    /*['meta', { name: 'theme-color', content: '#00baff' }],*/
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['link', { rel: 'icon', href: 'favicon.ico' }]
  ],

  markdown: {
    toc:  {
      includeLevel: [2, 3]
    },
    lineNumbers: true,
    extendMarkdown: md => {
      // use more markdown-it plugins!
      md.use(require('markdown-it-footnote'))
      md.use(require('markdown-it-deflist'))
    }
  },

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: true,
    logo: '/assets/logo-128x64.png',
    sidebarDepth: 1,
    nav: [
      {
        text: 'User Manual',
        link: '/manual/',
      },
      {
        text: 'Downloads',
        link: '/download/',
      },
      {
        text: 'Resources',
        link: '/resources/',
      },
      {
         text: 'Contributing',
         link: '/contributing/',
       },
      {
        text: 'Rig @ GitLab',
        link: 'https://gitlab.com/scce/Rig'
      }
    ],
    sidebar: {
      '/manual/': [
        {
          title: 'User Manual',
          collapsable: false,
          children: [
            '',
            'getting-started',
            'parameterization',
            'workflow-elements',
            'examples',
          ]
        }
      ],
      '/contributing/': [
        {
          title: 'Contributing',
          collapsable: false,
          children: [
            '',
            'building-app',
            'building-docs',
          ]
        }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    'img-lazy'
  ]
}
