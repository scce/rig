# Downloads

::: warning
You need Java 11 (or newer) installed on your system in order to run Rig.
:::

::: warning
**MacOS** users, please take note of [How to Run Rig on MacOS](#how-to-run-rig-on-macos)!
:::

The following versions of Rig are available for download from our servers:

<!-- 
## <version> (<iso-date>)

* [Rig <version> for Windows](https://ls5download.cs.tu-dortmund.de/rig/release/v<version>/info.scce.rig.product.id-win32.win32.x86_64.zip)
* [Rig <version> for Linux](https://ls5download.cs.tu-dortmund.de/rig/release/v<version>/info.scce.rig.product.id-linux.gtk.x86_64.zip)
* [Rig <version> for macOS](https://ls5download.cs.tu-dortmund.de/rig/release/v<version>/info.scce.rig.product.id-macosx.cocoa.x86_64.zip)

**Description**
<add release notes>
-->

## [1.0.3 (2023-01-10)](https://gitlab.com/scce/rig/-/releases/v1.0.3)

* [Rig 1.0.3 for Windows](https://ls5download.cs.tu-dortmund.de/rig/release/v1.0.3/info.scce.rig.product.id-win32.win32.x86_64.zip)
* [Rig 1.0.3 for Linux](https://ls5download.cs.tu-dortmund.de/rig/release/v1.0.3/info.scce.rig.product.id-linux.gtk.x86_64.zip)
* [Rig 1.0.3 for macOS](https://ls5download.cs.tu-dortmund.de/rig/release/v1.0.3/info.scce.rig.product.id-macosx.cocoa.x86_64.zip)

**Description**

* Update CINCO Version
* Remove confusing content-assists for Except, Only and Variable Expressions (see #56)
* Fix auto-saving failing when folder does not exist (see #46)
* Add ability to re-order properties and parameters with drag & drop (see #53)
* Add branding to Rig (see #13)
* Improvements to the code for maintainability and future work:
	* Replaced hooks with events (as part of #53)
	* Cleaned up plugin.xml (see #45)
	* Cleaned up CPD (see #55)
	* Cleaned up MGL as part of #56

## [1.0.2 (2022-07-05)](https://gitlab.com/scce/rig/-/releases/v1.0.2)

* [Rig 1.0.2 for Windows](https://ls5download.cs.tu-dortmund.de/rig/release/v1.0.2/info.scce.rig.product.id-win32.win32.x86_64.zip)
* [Rig 1.0.2 for Linux](https://ls5download.cs.tu-dortmund.de/rig/release/v1.0.2/info.scce.rig.product.id-linux.gtk.x86_64.zip)
* [Rig 1.0.2 for macOS](https://ls5download.cs.tu-dortmund.de/rig/release/v1.0.2/info.scce.rig.product.id-macosx.cocoa.x86_64.zip)

**Description**

* Update CINCO Version
* Remove Manhattan edge style (not supported in CINCO 2.1 anymore, see #39)
* Auto-generate configuration on model save (see #33)
* Prevent code generation when CycleCheck fails (see #26)
* Remove underscore in generated filename (see #20)
* Add missing `job:when` to context menu (see #32)

## 1.0.1 (2021-10-12)

* [Rig 1.0.1 for Windows](https://ls5download.cs.tu-dortmund.de/rig/legacy/Rig-1.1-win32.win32.x86_64.zip)
* [Rig 1.0.1 for Linux](https://ls5download.cs.tu-dortmund.de/rig/legacy/Rig-1.1-linux.gtk.x86_64.zip)
* [Rig 1.0.1 for macOS](https://ls5download.cs.tu-dortmund.de/rig/legacy/rig-1.0.1-macos.dmg)

**Description**

* Remove overly strict validation of script arguments; they are allowed to be configured locally (see #12)
* Add feature flag for stageless pipelines (see #11)
* Hide StageAssignmentStrategy (see #6), complete removal planned for later (see #14)

## 1.0.0 (2021-09-28)

* [Rig 1.0.0 for Windows](https://ls5download.cs.tu-dortmund.de/rig/legacy/Rig-1-win32.win32.x86_64.zip)
* [Rig 1.0.0 for Linux](https://ls5download.cs.tu-dortmund.de/rig/legacy/Rig-1-linux.gtk.x86_64.zip)
* ~~Rig 1.0.0 for macOS~~

**Description**

This is the initial public release of Rig 1.0.0.


## How to Run Rig on MacOS

Due to recent security changes on MacOS, you might run into the following error
message when trying to execute Rig on MacOS:

<center><img :src="$withBase('/assets/rig-macos-quarantine.png')" class="img-fluid" alt="'Rig is damaged' dialog on MacOS"></center>

Since we do not sign the executable, MacOS will quarantine the application and 
not allow it to run. You can verify this with by running the command 
`xattr Rig-1.202207051330.app com.apple.quarantine` on the command line.

You can remove the flag with the following command (adjust file name as needed):

````
% xattr -d com.apple.quarantine -r Rig-1.202207051330.app
````

This will allow you to run the application without trouble.

If you have concern about the integrity of our build, you can browse the sources
in our GitHub Repository and ether build the application yourself, or review our 
build pipelines there as well.