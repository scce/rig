/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.event

import graphmodel.ModelElementContainer
import info.scce.rig.graphmodel.controller.SlottableLayout
import info.scce.rig.pipeline.Slot
import info.scce.rig.pipeline.event.InternalSlotEvent

final class SlotEvent extends InternalSlotEvent {
	
	override postCreate(Slot element) {
		element.getContainer().setMinimized(false);
		SlottableLayout.layout(element.getContainer(), null);
	}
	
	override preDelete(Slot element) {
		SlottableLayout.layout(element.getContainer(), element);
	}
	
	override postMove(Slot element, ModelElementContainer oldContainer, int oldX, int oldY) {
		SlottableLayout.layout(element.getContainer(), null);
	}
}
