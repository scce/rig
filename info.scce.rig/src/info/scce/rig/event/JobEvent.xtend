/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.event

import info.scce.rig.pipeline.Job
import info.scce.rig.pipeline.event.InternalJobEvent
import java.util.Random

import static info.scce.rig.graphmodel.controller.UpdateStageIdx.updateStageIdxPerJob

final class JobEvent extends InternalJobEvent {
	
	static val String[] VERBS = #[
			"Build", "Test", "Lint", "Scan", "Analyze", 
			"Benchmark", "Deploy", "Generate", "Upload"
		];
		
	static val String[] NOUNS = #[
		"REST API", "SPA", "Website", "Client", "Desktop", "Server", "Services", 
		"Service", "Documentation", "Backend", "Infrastructure", 
		"Awesomeness", "Internet", "Unicorn"
	];
	
	static val Random random = new Random();
	
	def String randomJobName() {
		return String.format("%s %s", 
				VERBS.get(random.nextInt(VERBS.length)),
				NOUNS.get(random.nextInt(NOUNS.length)));
	}
	 
	override postCreate(Job element) {
		/* Update stage index */
		val container = element.getContainer();
		updateStageIdxPerJob(container);
		
		element.setName(randomJobName());
		super.postCreate(element)
	}
}
