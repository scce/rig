/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.event

import info.scce.rig.pipeline.Target
import info.scce.rig.pipeline.event.InternalTargetEvent
import java.util.Random

final class TargetEvent extends InternalTargetEvent {
	
	// use platforms for now, its enough as an example
	static val PLATFORM = #["OSX", "Windows", "Android", "XboxOne", "iOS", "Linux"];
	
	static val Random random = new Random();
	
	override postCreate(Target element) {
		element.setName(PLATFORM.get(random.nextInt(PLATFORM.length)));
		super.postCreate(element)
	}
}