/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.graphmodel.controller;

import java.util.stream.Collectors;

import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Slot;
import info.scce.rig.pipeline.Slottable;
import info.scce.rig.pipeline.Target;

public class SlottableLayout {
	
	public static final int HEADER_HEIGHT = 30;
	public static final int FOOTER_HEIGHT = 20;
	
	public static final int MIN_WIDTH = 200;
	public static final int MAX_WIDTH = 500;
	
	private static boolean suppress = false;
	
	public static final int clampWidth (int width) {
		return Math.min(MAX_WIDTH, Math.max(MIN_WIDTH, width));
	}
	
	public static void layout(Slottable slottable) {
		layout(slottable, null);
	}
	
	public static void layout (Slottable slottable, Slot ignore) {
		if (suppress) return;
		
		int width = clampWidth(slottable.getWidth());
		int height = HEADER_HEIGHT;
		
		var slots = slottable.getNodes(Slot.class).stream()
				.sorted((a, b) -> a.getY() - b.getY())
				.collect(Collectors.toList());
		
		for (Slot slot : slots) {
			if (slot == ignore) continue;
			
			boolean hasConnection = !slot.getIncoming().isEmpty() || !slot.getOutgoing().isEmpty();
			
			if (hasConnection || !slottable.isMinimized()) {
				// setWidth does not correctly update text positions, use resize
				slot.resize(width, slot.getHeight());
				suppress = true;
				slot.move(0, height);
				suppress = false;
				height += slot.getHeight() + 5;
				// we need to add padding here to have a space for dropping the element when re-ordering
				// a minimum of five is needed, otherwise the element always snaps to the snapping guidelines
				// making dropping it into the space between impossible.

			} else {
				suppress = true;
				slot.move(0, -slot.getHeight());
				suppress = false;
			}
		}
		
		if (slottable instanceof Job || slottable instanceof Target 
				|| (height == HEADER_HEIGHT && !slottable.isMinimized()))
			height += FOOTER_HEIGHT;
		
		suppress = true;
		slottable.resize (width, height);
		suppress = false;
	}
}
