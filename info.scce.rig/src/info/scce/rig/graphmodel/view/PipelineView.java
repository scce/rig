/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.graphmodel.view;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Target;

public class PipelineView {

	public static Set<Target> getReachableTargets(Job job, Map<Job, Set<Target>> cache) {
		if (!cache.containsKey(job)) {

			/* Under-approximation to prevent cyclic invocations */
			Set<Target> reachableTargets = new HashSet<Target>();
			cache.put(job, reachableTargets);

			/* Add directly reachable targets */
			reachableTargets.addAll(job.getTargetSuccessors());

			/* Add indirectly reachable targets */
			for (Job j : job.getJobSuccessors())
				reachableTargets.addAll(getReachableTargets(j, cache));
		}
		return cache.get(job);
	}
	
	/**
	 * Recursively gets all predecessors in the ancestry tree of this job
	 * @param job
	 * @return
	 */
	public static List<Job> getJobAncestors (Job job) {
		return 
			Stream.concat(
					Stream.of(job),
					job.getJobPredecessors().stream()
						.flatMap(j -> getJobAncestors(j).stream()))
			.collect(Collectors.toList());
	}
}
