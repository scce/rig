/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.pipeline.Description;
import info.scce.rig.pipeline.Milestone;
import info.scce.rig.pipeline.Name;
import info.scce.rig.pipeline.RawRef;
import info.scce.rig.pipeline.Release;
import info.scce.rig.pipeline.ReleasedAt;
import info.scce.rig.pipeline.TagName;
import info.scce.rig.pipeline.Target;

final class ReleaseGenerator extends AbstractJsonNodeGenerator<Release>{

	public ReleaseGenerator(ObjectMapper mapper) {
		super(mapper);
	}

	public JsonNode generate(Release subject, Target target) {
		StringPropertyMapper<Release> factory = new StringPropertyMapper<>(subject, target);
		ObjectNode node = mapper.createObjectNode();

		factory.value(Name.class)
			.ifPresent(value -> node.put("name", value));

		factory.value(TagName.class)
			.ifPresent(value -> node.put("tag_name", value));

		factory.value(Description.class)
			.ifPresent(value -> node.put("description", value));

		factory.value(RawRef.class)
			.ifPresent(value -> node.put("ref", value));

		List<String> milestones = factory.values(Milestone.class);
		if (!milestones.isEmpty())
			node.set("milestones", mapper.valueToTree(milestones));


		factory.value(ReleasedAt.class)
			.ifPresent(value -> node.put("released_at", value));

		return node;
	}
}
