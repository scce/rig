/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.BooleanPropertyMapper;
import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.generator.property.ValuePropertyMapper;
import info.scce.rig.pipeline.Cache;
import info.scce.rig.pipeline.File;
import info.scce.rig.pipeline.Key;
import info.scce.rig.pipeline.Path;
import info.scce.rig.pipeline.Policy;
import info.scce.rig.pipeline.Prefix;
import info.scce.rig.pipeline.Target;
import info.scce.rig.pipeline.Untracked;
import info.scce.rig.pipeline.When;

final class CacheGenerator extends AbstractJsonNodeGenerator<Cache> {

	public CacheGenerator(ObjectMapper mapper) {
		super(mapper);
	}

	public JsonNode generate(Cache subject, Target target) {
		StringPropertyMapper<Cache> stringFactory = new StringPropertyMapper<>(subject, target);
		ValuePropertyMapper<Cache> valueFactory = new ValuePropertyMapper<>(subject);
		BooleanPropertyMapper<Cache> booleanFactory = new BooleanPropertyMapper<>(subject, target);
		
		ObjectNode node = mapper.createObjectNode();

		stringFactory.value(Key.class)
			.ifPresent(value -> node.put("key", value));

		// cache:key:[files,prefix]
		if (subject.getKeys().isEmpty()) {
			ObjectNode key = mapper.createObjectNode();

			stringFactory.value(Prefix.class)
			.ifPresent(value -> key.put("prefix", value));

			List<String> files = stringFactory.values(File.class);
			if (!files.isEmpty())
				key.set("files", mapper.valueToTree(files));

			if (key.elements().hasNext())
				node.set("key", key);
		}

		List<String> paths = stringFactory.values(Path.class);
		if (!paths.isEmpty())
			node.set("paths", mapper.valueToTree(paths));

		booleanFactory.value(Untracked.class)
			.ifPresent(value -> node.put("untracked", value));

		valueFactory.value(When.class, String.class, when -> when.getValue().toString())
			.ifPresent(value -> node.put("when", value));

		valueFactory.value(Policy.class, String.class, policy -> policy.getValue().toString().replace("_", "-"))
			.ifPresent(value -> node.put("policy", value));

		return node;
	}
}
