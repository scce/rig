/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator.property;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import graphmodel.ModelElementContainer;
import info.scce.rig.pipeline.Property;
import info.scce.rig.pipeline.StringAssignment;
import info.scce.rig.pipeline.StringParameter;
import info.scce.rig.pipeline.StringProperty;
import info.scce.rig.pipeline.StringVariable;
import info.scce.rig.pipeline.Target;

public class StringPropertyMapper<C extends ModelElementContainer> {
	
	private final C subject;
	private final Target target;
	
	public StringPropertyMapper(C container, Target target) {
		this.subject = container;
		this.target = target;
	}
	
	public <P extends StringProperty> Optional<String> value(Class<P> property) {
		return _values(property).findFirst();
	}
	
	public <P extends StringProperty> List<String> values(Class<P> property) {
		return _values(property).collect(Collectors.toList());
	}
	
	private <P extends StringProperty> Stream<String> _values(Class<P> property) {
		
		Stream<String> variables = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(StringAssignment.class).stream())
				.map(StringAssignment::getSourceElement)
				.filter(node -> node instanceof StringVariable)
				.map(StringVariable.class::cast)
				.map(StringVariable::getValue);
		
		Stream<String> parameters = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(StringAssignment.class).stream())
				.map(StringAssignment::getSourceElement)
				.filter(node -> node instanceof StringParameter)
				.map(StringParameter.class::cast)
				.filter(parameter -> parameter.getContainer() == target)
				.map(StringParameter::getValue);
		
		Stream<String> values = subject.getNodes(property).stream()
				.filter(p -> p.getIncoming().size() == 0)
				.map(StringProperty::getValue);
		
		return Stream.of(parameters, variables, values)
				.reduce(Stream::concat)
				.orElseGet(Stream::empty);
	}
	
	public <P extends Property> List<String> 
	mappedStringValues(Class<P> property, Function<P, String> extractor) {
		return _mappedStringValues(property, extractor).collect(Collectors.toList());
	}
	
	private <P extends Property> Stream<String> 
	_mappedStringValues(Class<P> property, Function<P, String> extractor) {
		
		Stream<String> variables = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(StringAssignment.class).stream())
				.map(StringAssignment::getSourceElement)
				.filter(node -> node instanceof StringVariable)
				.map(StringVariable.class::cast)
				.map(StringVariable::getValue);
		
		Stream<String> parameters = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(StringAssignment.class).stream())
				.map(StringAssignment::getSourceElement)
				.filter(node -> node instanceof StringParameter)
				.map(StringParameter.class::cast)
				.filter(parameter -> parameter.getContainer() == target)
				.map(StringParameter::getValue);
		
		Stream<String> values = subject.getNodes(property).stream()
				.filter(p -> p.getIncoming().size() == 0)
				.map(extractor);
		
		return Stream.of(parameters, variables, values)
				.reduce(Stream::concat)
				.orElseGet(Stream::empty);
	}
	
}
