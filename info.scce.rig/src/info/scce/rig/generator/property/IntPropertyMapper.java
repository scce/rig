/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator.property;

import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import graphmodel.ModelElementContainer;
import info.scce.rig.pipeline.IntAssignment;
import info.scce.rig.pipeline.IntParameter;
import info.scce.rig.pipeline.IntProperty;
import info.scce.rig.pipeline.IntVariable;
import info.scce.rig.pipeline.Target;

public class IntPropertyMapper<C extends ModelElementContainer> {
	
	private final C subject;
	private final Target target;
	
	public IntPropertyMapper(C container, Target target) {
		this.subject = container;
		this.target = target;
	}
	
	public <P extends IntProperty> OptionalInt value(Class<P> property) {
		return _values(property).findFirst();
	}
	
	public <P extends IntProperty> List<Integer> values (Class<P> property) {
		return _values(property)
			.mapToObj(i -> i)
			.collect(Collectors.toList());
	}
	
	private <P extends IntProperty> IntStream _values(Class<P> property) {
		
		IntStream variables = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(IntAssignment.class).stream())
				.map(IntAssignment::getSourceElement)
				.filter(node -> node instanceof IntVariable)
				.map(IntVariable.class::cast)
				.mapToInt(IntVariable::getValue);
		
		IntStream parameters = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(IntAssignment.class).stream())
				.map(IntAssignment::getSourceElement)
				.filter(node -> node instanceof IntParameter)
				.map(IntParameter.class::cast)
				.filter(parameter -> parameter.getContainer() == target)
				.mapToInt(IntParameter::getValue);
		
		IntStream values = subject.getNodes(property).stream()
				.filter(p -> p.getIncoming().size() == 0)
				.mapToInt(IntProperty::getValue);
		
		return Stream.of(parameters, variables, values)
				.reduce(IntStream::concat)
				.orElseGet(IntStream::empty);
	}

}
