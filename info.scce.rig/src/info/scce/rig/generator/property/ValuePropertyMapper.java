/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator.property;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import graphmodel.ModelElementContainer;
import info.scce.rig.pipeline.Property;

public class ValuePropertyMapper<C extends ModelElementContainer> {
	
	private final C subject;
	
	public ValuePropertyMapper(C container) {
		this.subject = container;
	}

	public <P extends Property, V> Optional<V> value(Class<P> property, Class<V> clazz, Function<P, V> extractor) {
		return subject.getNodes(property).stream()
			.map(extractor)
			.findFirst();
	}
	
	public <P extends Property> List<String> values(Class<P> property, Function<P, String> extractor) {
		return subject.getNodes(property).stream()
			.map(extractor)
			.collect(Collectors.toList());
	}

}
