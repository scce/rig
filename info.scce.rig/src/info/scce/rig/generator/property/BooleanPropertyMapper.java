/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator.property;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import graphmodel.ModelElementContainer;
import info.scce.rig.pipeline.BooleanAssignment;
import info.scce.rig.pipeline.BooleanParameter;
import info.scce.rig.pipeline.BooleanProperty;
import info.scce.rig.pipeline.BooleanVariable;
import info.scce.rig.pipeline.Target;

public class BooleanPropertyMapper<C extends ModelElementContainer> {
	
	private final C subject;
	private final Target target;
	
	public BooleanPropertyMapper(C container, Target target) {
		this.subject = container;
		this.target = target;
	}
	
	
	public <P extends BooleanProperty> Optional<Boolean> value(Class<P> property) {
		return _values(property).findFirst();
	}
	
	public <P extends BooleanProperty> List<Boolean> values(Class<P> property) {
		return _values(property).collect(Collectors.toList());
	}
	
	private <P extends BooleanProperty> Stream<Boolean> _values(Class<P> property) {
		
		Stream<Boolean> variables = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(BooleanAssignment.class).stream())
				.map(BooleanAssignment::getSourceElement)
				.filter(node -> node instanceof BooleanVariable)
				.map(BooleanVariable.class::cast)
				.map(BooleanVariable::isValue);
		
		Stream<Boolean> parameters = subject.getNodes(property).stream()
				.flatMap(p -> p.getIncoming(BooleanAssignment.class).stream())
				.map(BooleanAssignment::getSourceElement)
				.filter(node -> node instanceof BooleanParameter)
				.map(BooleanParameter.class::cast)
				.filter(parameter -> parameter.getContainer() == target)
				.map(BooleanParameter::isValue);
		
		Stream<Boolean> values = subject.getNodes(property).stream()
				.filter(p -> p.getIncoming().size() == 0)
				.map(BooleanProperty::isValue);
		
		return Stream.of(parameters, variables, values)
				.reduce(Stream::concat)
				.orElseGet(Stream::empty);

	}
}
