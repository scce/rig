/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.pipeline.EntryPoint;
import info.scce.rig.pipeline.Image;
import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Name;
import info.scce.rig.pipeline.SimpleImage;
import info.scce.rig.pipeline.Target;

final class ImagesGenerator extends AbstractGenerator{

	public ImagesGenerator(ObjectMapper mapper) {
		super(mapper);
	}

	public Optional<JsonNode> generate(Optional<String> image, Job job, Target target) {

		Stream<JsonNode> images =
			// Optional.stream() is java 9
			(image.isPresent() ? Stream.of(image.get()) : Stream.<String> empty())
				.map(string -> {
					ObjectNode img = mapper.createObjectNode();
					img.put("name", string);
					return img;
				});

		return Stream.concat(images, Stream.concat(
			target.getSimpleImages().stream()
				.map(this::generateSimpleImage),
			Stream.of(
					job.getImagePredecessors().stream(),
					target.getImagePredecessors().stream())
			.reduce(Stream::concat)
			.orElseGet(Stream::empty)
			.map(img -> generateImage(img, target))
		))
		.findFirst();
	}
	
	// for target
	private JsonNode generateSimpleImage(SimpleImage subject) {
		ObjectNode node = mapper.createObjectNode();
		node.put("name", subject.getValue());
		return node;
	}

	private JsonNode generateImage(Image subject, Target target) {
		StringPropertyMapper<Image> stringFactory = new StringPropertyMapper<>(subject, target);
		ObjectNode node = mapper.createObjectNode();

		stringFactory.value(Name.class).ifPresent(value -> node.put("name", value));

		List<String> entrypoint = stringFactory.values(EntryPoint.class);
		if (!entrypoint.isEmpty())
			node.set("entrypoint", mapper.valueToTree(entrypoint));

		return node;
	}
}
