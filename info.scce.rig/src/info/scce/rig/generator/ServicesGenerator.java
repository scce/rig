/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.List;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.pipeline.Alias;
import info.scce.rig.pipeline.Command;
import info.scce.rig.pipeline.EntryPoint;
import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Name;
import info.scce.rig.pipeline.Service;
import info.scce.rig.pipeline.SimpleService;
import info.scce.rig.pipeline.Target;

final class ServicesGenerator extends AbstractGenerator{

	public ServicesGenerator(ObjectMapper mapper) {
		super(mapper);
	}

	public ArrayNode generate(StringPropertyMapper<Job> factory, Job job, Target target) {
		ArrayNode services = mapper.createArrayNode();

		Stream.concat(Stream.concat(
			factory.values(SimpleService.class).stream().map(string -> {
				ObjectNode _svc = mapper.createObjectNode();
				_svc.put("name", string);
				return _svc;
			}),
			target.getSimpleServices().stream().map(this::generateSimpleService)),
			Stream.of(
					job.getServicePredecessors().stream(),
					target.getServicePredecessors().stream())
				.reduce(Stream::concat)
				.orElseGet(Stream::empty)
				.map(svc -> generateService(svc, target)))
				.forEach(services::add);
		return services;
	}
	
	// for target
	private JsonNode generateSimpleService(SimpleService subject) {
		ObjectNode node = mapper.createObjectNode();
		node.put("name", subject.getValue());
		return node;
	}
	
	private JsonNode generateService(Service subject, Target target) {
		StringPropertyMapper<Service> factory = new StringPropertyMapper<>(subject, target);
		ObjectNode node = mapper.createObjectNode();

		factory.value(Name.class)
			.ifPresent(value -> node.put("name", value));

		factory.value(Alias.class)
			.ifPresent(value -> node.put("alias", value));

		List<String> commands = factory.values(Command.class);
		if (!commands.isEmpty())
			node.set("command", mapper.valueToTree(commands));

		List<String> entrypoint = factory.values(EntryPoint.class);
		if (!entrypoint.isEmpty())
			node.set("entrypoint", mapper.valueToTree(entrypoint));

		return node;
	}

	
	
}
