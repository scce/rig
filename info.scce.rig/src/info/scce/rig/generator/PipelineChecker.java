/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import info.scce.rig.checks.CycleCheck;
import info.scce.rig.pipeline.Pipeline;

final class PipelineChecker {
	
	class PipelineCheckerException extends Exception{

		private static final long serialVersionUID = 1L;

		public PipelineCheckerException(String message) {
			super(message);
		}
		
	}
	
	private final CycleCheck cycleCheck;
	
	public PipelineChecker(CycleCheck cycleCheck) {
		this.cycleCheck = cycleCheck;
	}
	
	public void check(Pipeline model) throws PipelineCheckerException{
		if (cycleCheck.isCyclic(model)) {
			throw new PipelineCheckerException("Cycle in the pipeline.");
		}
	}

}
