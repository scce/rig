/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.stream.Stream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.BooleanPropertyMapper;
import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.pipeline.Change;
import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Kubernetes;
import info.scce.rig.pipeline.Only;
import info.scce.rig.pipeline.Ref;
import info.scce.rig.pipeline.SimpleOnly;
import info.scce.rig.pipeline.Target;
import info.scce.rig.pipeline.VarExp;

final class OnlyGenerator extends AbstractGenerator{
	
	public OnlyGenerator(ObjectMapper mapper) {
		super(mapper);
		// TODO Auto-generated constructor stub
	}

	public JsonNode generate(StringPropertyMapper<Job> factory, Job job, Target target) {
		ObjectNode node = mapper.createObjectNode();

		ArrayNode ref = mapper.createArrayNode();
		ArrayNode exp = mapper.createArrayNode();
		ArrayNode change = mapper.createArrayNode();

		factory.mappedStringValues(SimpleOnly.class, SimpleOnly::getValue)
			.forEach(ref::add);

		target.getSimpleOnlys().stream()
			.map(SimpleOnly::getValue)
			.forEach(ref::add);

		Stream<Only> onlies = Stream.concat(
				job.getOnlyPredecessors().stream(),
				target.getOnlyPredecessors().stream());

		onlies.forEach(only -> {
			StringPropertyMapper<Only> stringSubject = new StringPropertyMapper<>(only, target);
			BooleanPropertyMapper<Only> booleanFactory = new BooleanPropertyMapper<>(only, target);
			
			stringSubject.mappedStringValues(Ref.class, Ref::getValue).forEach(ref::add);
			stringSubject.mappedStringValues(Change.class, Change::getValue).forEach(change::add);
			stringSubject.mappedStringValues(VarExp.class, VarExp::getValue).forEach(exp::add);

			// TODO: Conflicting kubernetes are not allowed
			booleanFactory.value(Kubernetes.class).ifPresent(value -> {
				if (value)
					node.put("kubernetes", "active");
			});
		});

		if (ref.size() > 0)
			node.set("refs", ref);

		if (exp.size() > 0)
			node.set("variables", exp);

		if(change.size() > 0)
			node.set("changes", change);

		return node;
	}
}
