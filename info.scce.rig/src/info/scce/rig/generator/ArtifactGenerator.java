/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.BooleanPropertyMapper;
import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.generator.property.ValuePropertyMapper;
import info.scce.rig.pipeline.Artifact;
import info.scce.rig.pipeline.Exclude;
import info.scce.rig.pipeline.ExpireIn;
import info.scce.rig.pipeline.ExposeAs;
import info.scce.rig.pipeline.Name;
import info.scce.rig.pipeline.Path;
import info.scce.rig.pipeline.Report;
import info.scce.rig.pipeline.Target;
import info.scce.rig.pipeline.Untracked;
import info.scce.rig.pipeline.When;

final class ArtifactGenerator extends AbstractJsonNodeGenerator<Artifact> {
	
	public ArtifactGenerator(ObjectMapper mapper) {
		super(mapper);
	}

	public JsonNode generate(Artifact subject, Target target) {
		StringPropertyMapper<Artifact> stringFactory = new StringPropertyMapper<>(subject, target);
		ValuePropertyMapper<Artifact> valueFactory = new ValuePropertyMapper<>(subject);
		BooleanPropertyMapper<Artifact> booleanFactory = new BooleanPropertyMapper<>(subject, target);
		
		ObjectNode node = mapper.createObjectNode();

		stringFactory.value(Name.class)
			.ifPresent(value -> node.put("name", value));
		stringFactory.value(ExposeAs.class)
			.ifPresent(value -> node.put("expose_as", value));
		stringFactory.value(ExpireIn.class)
		.ifPresent(value -> node.put("expire_in", value));

		List<String> paths = stringFactory.values(Path.class);
		if (!paths.isEmpty())
			node.set("paths", mapper.valueToTree(paths));

		List<String> exclude = stringFactory.values(Exclude.class);
		if (!exclude.isEmpty())
			node.set("exclude", mapper.valueToTree(exclude));

		valueFactory.value(When.class, String.class,
				when -> when.getValue().toString())
			.ifPresent(value -> node.put("when", value));

		if (!subject.getReports().isEmpty()) {
			ObjectNode reportNode = mapper.createObjectNode();
			subject.getReports().stream()
			.collect(Collectors.groupingBy(
				report -> report.getKind(),
				Collectors.mapping(Report::getValue, Collectors.toList())))
			.forEach((key, val) -> reportNode.set(key.toString(), mapper.valueToTree(val)));
			node.set("reports", reportNode);
		}

		booleanFactory.value(Untracked.class)
			.ifPresent(value -> node.put("untracked", value));
		return node;
	}

}
