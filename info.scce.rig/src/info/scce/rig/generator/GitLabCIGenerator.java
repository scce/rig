/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import de.jabc.cinco.meta.core.utils.job.JobFactory;
import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator;
import info.scce.rig.generator.ConfigurationGenerator.ConfigurationGeneratorException;
import info.scce.rig.pipeline.Pipeline;

public final class GitLabCIGenerator implements IGenerator<Pipeline> {

	private static final String GITLAB_CI_FILENAME = ".gitlab-ci.yml";
	private final ConfigurationGenerator configurationGenerator;
	
	public GitLabCIGenerator(){
		this.configurationGenerator = new ConfigurationGenerator();
	}
	
	@Override
	public void generate(Pipeline model, IPath path, IProgressMonitor monitor) {
		File targetFile = path.append(GITLAB_CI_FILENAME).toFile();

		Job generate = Job
				.create(
						"Generating...",
						(IProgressMonitor progressMonitor) -> {
							try (PrintWriter writer = new PrintWriter(targetFile)) {
								configurationGenerator.generate(model, writer);
								return Status.OK_STATUS;
							} catch (FileNotFoundException | ConfigurationGeneratorException e) {
								return new Status(Status.ERROR, "unknown", "Generation failed...", e);
							}
						}
				);
		
		JobFactory
			.job("Rigging the workflow", monitor, false)
			.consume(1)
			.task(generate)
			.schedule();
	}

}
