/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.List;
import java.util.stream.Stream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.BooleanPropertyMapper;
import info.scce.rig.generator.property.ValuePropertyMapper;
import info.scce.rig.pipeline.AllowFailure;
import info.scce.rig.pipeline.Change;
import info.scce.rig.pipeline.Exists;
import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Rule;
import info.scce.rig.pipeline.Target;
import info.scce.rig.pipeline.VarExp;
import info.scce.rig.pipeline.When;

final class RulesGenerator extends AbstractJsonNodeGenerator<Job>{

	public RulesGenerator(ObjectMapper mapper) {
		super(mapper);
	}

	public JsonNode generate(Job job, Target target) {
		ArrayNode array = mapper.createArrayNode();

		Stream.concat(
			job.getRulePredecessors().stream(),
			target.getRulePredecessors().stream()
		)
		.map(rule -> generateRule(rule, target))
		.forEach(array::add);

		return array;
	}

	private JsonNode generateRule(Rule rule, Target target) {
		ObjectNode node = mapper.createObjectNode();
		ValuePropertyMapper<Rule> valueFactory = new ValuePropertyMapper<Rule>(rule);
		BooleanPropertyMapper<Rule> factoryBoolean = new BooleanPropertyMapper<Rule>(rule, target);

		valueFactory.value(VarExp.class, String.class, p -> p.getValue().toString())
			.ifPresent(value -> node.put("if", value));

		List<String> changes = valueFactory.values(Change.class, Change::getValue);
		if (!changes.isEmpty())
			node.set("changes", mapper.valueToTree(changes));

		List<String> exists = valueFactory.values(Exists.class, Exists::getValue);
		if (!exists.isEmpty())
			node.set("exists", mapper.valueToTree(exists));

		valueFactory.value(When.class, String.class, p -> p.getValue().toString())
			.ifPresent(value -> node.put("when", value));

		factoryBoolean.value(AllowFailure.class)
			.ifPresent(value -> node.put("allow_failure", value));

		return node;
	}
}
