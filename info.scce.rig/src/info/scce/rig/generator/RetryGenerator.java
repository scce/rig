/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.IntPropertyMapper;
import info.scce.rig.generator.property.ValuePropertyMapper;
import info.scce.rig.pipeline.Retry;
import info.scce.rig.pipeline.RetryWhen;
import info.scce.rig.pipeline.SimpleRetry;
import info.scce.rig.pipeline.Target;

final class RetryGenerator extends AbstractJsonNodeGenerator<Retry>{

	public RetryGenerator(ObjectMapper mapper) {
		super(mapper);
	}

	public JsonNode generate(Retry subject, Target target) {
		IntPropertyMapper<Retry> intFactory = new IntPropertyMapper<>(subject, target);
		ValuePropertyMapper<Retry> valueFactory = new ValuePropertyMapper<Retry>(subject);
		ObjectNode node = mapper.createObjectNode();

		intFactory.value(SimpleRetry.class)
			.ifPresent(value -> node.put("max", value));

		valueFactory.value(RetryWhen.class, String.class, when -> when.getValue().toString())
			.ifPresent(value -> node.put("when", value));

		return node;
	}
}
