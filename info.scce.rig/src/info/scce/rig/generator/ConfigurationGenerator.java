/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.format.DateTimeFormatter;
import java.util.SortedSet;
import java.util.TreeSet;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import info.scce.rig.checks.CycleCheck;
import info.scce.rig.generator.PipelineChecker.PipelineCheckerException;
import info.scce.rig.pipeline.Pipeline;
import info.scce.rig.pipeline.Target;

final class ConfigurationGenerator {
	
	class ConfigurationGeneratorException extends Exception{

		private static final long serialVersionUID = 1L;

		public ConfigurationGeneratorException(String message, Throwable cause) {
			super(message, cause);
		}
		
	}

	private final PipelineChecker pipelineChecker;
	private final ObjectMapper mapper;
	private final HeaderGenerator headerGenerator;
	private final TargetGenerator targetGenerator;
	private final VariablesGenerator variablesGenerator;
	private final StagesGenerator stagesGenerator;
	
	public ConfigurationGenerator(){
		pipelineChecker = new PipelineChecker(new CycleCheck());
		
		mapper = JsonMapper.builder(
				YAMLFactory.builder()
					.enable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
					.build()
			)
			.addModule(new Jdk8Module())
			.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
			.serializationInclusion(JsonInclude.Include.NON_EMPTY)
			.serializationInclusion(Include.NON_ABSENT)
			.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
			.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false)
			.configure(JsonParser.Feature.AUTO_CLOSE_SOURCE, false)
			.build();
	
		headerGenerator = new HeaderGenerator(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		targetGenerator = new TargetGenerator(
				new JobGenerator(
						new JobNameGenerator(),
						new JobAttributesGenerator(
								mapper,
								new ArtifactGenerator(mapper), 
								new CacheGenerator(mapper), 
								new EnvironmentGenerator(mapper),
								new ExceptGenerator(mapper), 
								new ImagesGenerator(mapper), 
								new OnlyGenerator(mapper), 
								new ReleaseGenerator(mapper), 
								new RetryGenerator(mapper), 
								new RulesGenerator(mapper), 
								new ServicesGenerator(mapper)
						)
				)
		);
		variablesGenerator = new VariablesGenerator(mapper);
		stagesGenerator = new StagesGenerator(mapper);
	}
	
	public void generate(Pipeline model, PrintWriter writer) throws ConfigurationGeneratorException {
		ObjectNode pipeline = mapper.createObjectNode();
		SortedSet<String> stages = new TreeSet<>();

		try {
			pipelineChecker.check(model);

			headerGenerator.generate(writer);
			variablesGenerator.generate(model, pipeline);

			for (Target target : model.getTargets()) {
				targetGenerator.generate(target, pipeline, stages);
			}

			stagesGenerator.generate(model, pipeline, stages);
			mapper.writeValue(writer, pipeline);
		} catch (PipelineCheckerException | IOException e) {
			throw new ConfigurationGeneratorException(e.getMessage(), e);
		}
	}
}
