/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.generator.property.ValuePropertyMapper;
import info.scce.rig.pipeline.AutoStopIn;
import info.scce.rig.pipeline.EnvAction;
import info.scce.rig.pipeline.Environment;
import info.scce.rig.pipeline.Name;
import info.scce.rig.pipeline.Target;
import info.scce.rig.pipeline.URL;

final class EnvironmentGenerator extends AbstractJsonNodeGenerator<Environment> {

	public EnvironmentGenerator(ObjectMapper mapper) {
		super(mapper);
	}
	
	public JsonNode generate(Environment subject, Target target) {
		StringPropertyMapper<Environment> stringFactory = new StringPropertyMapper<>(subject, target);
		ValuePropertyMapper<Environment> valueFactory = new ValuePropertyMapper<>(subject);
		
		ObjectNode node = mapper.createObjectNode();

		stringFactory.value(Name.class)
			.ifPresent(value -> node.put("name", value));

		stringFactory.value(URL.class)
			.ifPresent(value -> node.put("url", value));

		stringFactory.value(AutoStopIn.class)
			.ifPresent(value -> node.put("auto_stop_in", value));

		valueFactory.value(EnvAction.class, String.class, action -> action.getValue().toString())
			.ifPresent(value -> node.put("action", value));

		return node;
	}
}
