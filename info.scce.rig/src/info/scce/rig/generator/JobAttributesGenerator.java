/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.generator;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import info.scce.rig.generator.property.BooleanPropertyMapper;
import info.scce.rig.generator.property.IntPropertyMapper;
import info.scce.rig.generator.property.StringPropertyMapper;
import info.scce.rig.generator.property.ValuePropertyMapper;
import info.scce.rig.pipeline.AllowFailure;
import info.scce.rig.pipeline.Artifact;
import info.scce.rig.pipeline.Cache;
import info.scce.rig.pipeline.Coverage;
import info.scce.rig.pipeline.Environment;
import info.scce.rig.pipeline.Interruptible;
import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Parallel;
import info.scce.rig.pipeline.Release;
import info.scce.rig.pipeline.ResourceGroup;
import info.scce.rig.pipeline.Retry;
import info.scce.rig.pipeline.SimpleImage;
import info.scce.rig.pipeline.SimpleRetry;
import info.scce.rig.pipeline.StringParameter;
import info.scce.rig.pipeline.Tag;
import info.scce.rig.pipeline.Target;
import info.scce.rig.pipeline.Timeout;
import info.scce.rig.pipeline.When;

final class JobAttributesGenerator extends AbstractJsonNodeGenerator<Job> {

	private final AbstractJsonNodeGenerator<Artifact> artifactGenerator;
	private final AbstractJsonNodeGenerator<Cache> cacheGenerator;
	private final AbstractJsonNodeGenerator<Environment> environmentGenerator;
	private final ExceptGenerator exceptGenerator;
	private final OnlyGenerator onlyGenerator;
	private final ImagesGenerator imagesGenerator;
	private final AbstractJsonNodeGenerator<Release> releaseGenerator;
	private final AbstractJsonNodeGenerator<Retry> retryGenerator;
	private final AbstractJsonNodeGenerator<Job> rulesGenerator;
	private final ServicesGenerator servicesGenerator;
		
	
	public JobAttributesGenerator(
			ObjectMapper mapper,
			ArtifactGenerator artifactGenerator,
			CacheGenerator cacheGenerator,
			EnvironmentGenerator environmentGenerator,
			ExceptGenerator exceptGenerator,
			ImagesGenerator imagesGenerator,
			OnlyGenerator onlyGenerator,
			ReleaseGenerator releaseGenerator,
			RetryGenerator retryGenerator,
			RulesGenerator rulesGenerator,
			ServicesGenerator servicesGenerator
	) {		
		super(mapper);
		this.artifactGenerator = artifactGenerator;
		this.cacheGenerator = cacheGenerator;
		this.releaseGenerator = releaseGenerator;
		this.environmentGenerator = environmentGenerator;
		this.exceptGenerator = exceptGenerator;
		this.retryGenerator = retryGenerator;
		this.onlyGenerator = onlyGenerator;
		this.servicesGenerator = servicesGenerator;
		this.imagesGenerator = imagesGenerator;
		this.rulesGenerator = rulesGenerator;
	}
	

	public JsonNode generate(Job job, Target target) {
		ObjectNode node = mapper.createObjectNode();

		if (job.getRootElement().isStageless())
			node.put("stage", String.format("stage-%s", job.getStageIdx()));

		if (job.getScriptArguments().size() > 0) {
			node.set("variables", getScriptArguments(job, target));
		}

		readScript(node, "before_script", job.getBefore_script());
		JsonNode script = mapper.valueToTree(listScript(job.getScript()));
		node.set("script", script);
		readScript(node, "after_script", job.getAfter_script());

		IntPropertyMapper<Job> intFactory = new IntPropertyMapper<>(job, target);
		StringPropertyMapper<Job> stringFactory = new StringPropertyMapper<>(job, target);
		ValuePropertyMapper<Job> valueFactory = new ValuePropertyMapper<>(job);
		BooleanPropertyMapper<Job> booleanFactory = new BooleanPropertyMapper<>(job, target);

		// TODO
//		, SimpleOnly     [0, *]
//		, SimpleExcept   [0, *]
//		, ScriptArgument [0, *]

		booleanFactory.value(AllowFailure.class)
			.ifPresent(value -> node.put("allow_failure", value));
		stringFactory.value(Coverage.class)
			.ifPresent(value -> node.put("coverage", value));
		booleanFactory.value(Interruptible.class)
			.ifPresent(value -> node.put("interruptible", value));
		intFactory.value(Parallel.class)
			.ifPresent(value -> node.put("parallel", value));
		stringFactory.value(ResourceGroup.class)
			.ifPresent(value -> node.put("resource_group", value));
		intFactory.value(SimpleRetry.class)
			.ifPresent(value -> node.put("retry", value));
		List<String> tags = stringFactory.values(Tag.class);
		if (!tags.isEmpty())
			node.set("tags", mapper.valueToTree(tags));
		stringFactory.value(Timeout.class)
			.ifPresent(value -> node.put("timeout", value));
		valueFactory.value(When.class, String.class, when -> when.getValue().toString())
			.ifPresent(value -> node.put("when", value));

//		, ExceptOnlyAssignment  [0, *]
//		, RuleAssignment        [0, *]

		// should only be one (per grammar)
		job.getArtifactPredecessors().forEach(subject ->
			node.set("artifacts", artifactGenerator.generate(subject, target)));

		// should only be one (per grammar)
		job.getCachePredecessors().forEach(subject ->
			node.set("cache", cacheGenerator.generate(subject, target)));

		// should only be one (per grammar)
		job.getReleasePredecessors().forEach(subject ->
			node.set("release", releaseGenerator.generate(subject, target)));

		// should only be one (per grammar)
		job.getEnvironmentPredecessors().forEach(subject ->
			node.set("environment", environmentGenerator.generate(subject, target)));

		// should only be one (per grammar)
		job.getRetryPredecessors().forEach(subject ->
			node.set("retry", retryGenerator.generate(subject, target)));


		ArrayNode services = servicesGenerator.generate(stringFactory, job, target);
		if (services.size() > 0)
			node.set("services", services);

		imagesGenerator.generate(stringFactory.value(SimpleImage.class), job, target)
			.ifPresent(value -> node.set("image", value));

		JsonNode except = exceptGenerator.generate(stringFactory, job, target);
		if (!except.isEmpty())
			node.set("except", except);

		JsonNode only = onlyGenerator.generate(stringFactory, job, target);
		if (!only.isEmpty())
			node.set("only", only);

		JsonNode rules = rulesGenerator.generate(job, target);
		if (!rules.isEmpty())
			node.set("rules", rules);

		List<String> needs = job.getJobPredecessors().stream()
			.map(predecessor -> String.format("%s@%s", predecessor.getName(), target.getName()))
			.collect(Collectors.toList());

		if (!needs.isEmpty())
			node.set("needs", mapper.valueToTree(needs));

		return node;
	}
	
	private JsonNode getScriptArguments (Job job, Target target) {
		ObjectNode args = mapper.createObjectNode();
		job.getScriptArguments().stream()
			.sorted((a, b) -> a.getY() - b.getY())
			.collect(Collectors.toList())
			.forEach(arg -> {
				String value = arg.getValue();
	
				if (arg.getStringVariablePredecessors().size() == 1)
					value = arg.getStringVariablePredecessors().get(0).getValue();
				else
					value = arg.getStringParameterPredecessors().stream()
					.filter(param -> param.getContainer() == target)
					.map(StringParameter::getValue)
					.findFirst()
					.orElse(value);
				args.put(arg.getKey(), value);
			});
		return args;
	}
	
	private void readScript(ObjectNode node, String key, String text) {
		if (text != null && !text.trim().isEmpty()) {
			JsonNode lines = mapper.valueToTree(listScript(text));
			node.set(key, lines);
		}
	}
	
	/**
	 * Split script into lines and perform cleanup
	 * (cf. https://gitlab.com/scce/cinco/-/issues/260)
	 *
	 * @param script
	 * @return
	 */
	private List<String> listScript(String script) {
		return Arrays.stream(script.split("\\R+"))
			.map(String::trim)
			.filter(not(String::isEmpty))
			.collect(Collectors.toList());
	}	
	
	// Java 11 has Predicate.not()
		private static <T> Predicate<T> not(Predicate<T> predicate) {
			return predicate.negate();
			//return t -> !predicate.test(t);
		}
}
