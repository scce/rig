/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.appearance;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;
import info.scce.rig.pipeline.Note;
import style.Appearance;
import style.Color;
import style.StyleFactory;

public class NoteAppearanceProvider implements StyleAppearanceProvider<Note> {

	@Override
	public Appearance getAppearance(Note note, String shape) {
		Appearance appearance = StyleFactory.eINSTANCE.createAppearance();
		switch (shape) {
			case "body":
				Color color = StyleFactory.eINSTANCE.createColor();
				List<Integer> rgb = Arrays.stream(note.getColor().split(","))
					.map(String::trim)
					.map(Integer::parseInt)
					.collect(Collectors.toList());
				color.setR(rgb.get(0));
				color.setG(rgb.get(1));
				color.setB(rgb.get(2));
				appearance.setBackground(color);
				break;
		}
		return appearance;
	}
}
