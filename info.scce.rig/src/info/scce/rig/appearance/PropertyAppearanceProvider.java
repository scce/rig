/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.appearance;

import de.jabc.cinco.meta.core.ge.style.generator.runtime.appearance.StyleAppearanceProvider;
import info.scce.rig.pipeline.Property;
import style.Appearance;
import style.StyleFactory;

public class PropertyAppearanceProvider implements StyleAppearanceProvider<Property> {

	@Override
	public Appearance getAppearance(Property property, String shape) {
		Appearance appearance = StyleFactory.eINSTANCE.createAppearance();
		switch (shape) {
			case "padlock":
				appearance.setTransparency(property.getIncoming().size() == 0 ? 1.0 : 0.0);
				break;
				
			case "propertyValue":
				appearance.setTransparency(property.getIncoming().size() == 0 ? 0.0 : 1.0);
				break;
		}
		return appearance;
	}
}
