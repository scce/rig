/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.start;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.navigator.INavigatorContentService;
import org.eclipse.ui.navigator.INavigatorFilterService;
import org.eclipse.ui.navigator.resources.ProjectExplorer;

public class OnStartPlugIn implements org.eclipse.ui.IStartup {

	
	//deactivtes all filters of the Project Explorer except the .project filter
	@Override
	public void earlyStartup() {
		Display.getDefault().asyncExec(() ->
	     {
	    	 IWorkbenchPart part = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage().findView(ProjectExplorer.VIEW_ID);

				INavigatorContentService contentService = (INavigatorContentService) part
						.getAdapter(INavigatorContentService.class);
				
				INavigatorFilterService filterServ = contentService.getFilterService();
				String[] enabledFilters = {".project"}; 
			    filterServ.activateFilterIdsAndUpdateViewer(enabledFilters);
	     });
		
	}

}
