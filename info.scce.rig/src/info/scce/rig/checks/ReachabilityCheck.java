/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.checks;

import static info.scce.rig.graphmodel.view.PipelineView.getReachableTargets;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import info.scce.rig.pipeline.Pipeline;
import info.scce.rig.pipeline.Job;
import info.scce.rig.pipeline.Target;
import info.scce.rig.mcam.modules.checks.PipelineCheck;

public class ReachabilityCheck extends PipelineCheck {

	@Override
	public void check(Pipeline model) {
		Map<Job, Set<Target>> cache = new HashMap<Job, Set<Target>>();
		for (Job j : model.getJobs()) {
			if (getReachableTargets(j, cache).isEmpty())
				addWarning(j, Job.class.getSimpleName() + " is never used");
		}
	}
}
