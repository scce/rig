/*-
 * #%L
 * Rig
 * %%
 * Copyright (C) 2022 TU Dortmund University - Department of Computer Science - Chair for Programming Systems
 * %%
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 2
 * with the GNU Classpath Exception which is
 * available at https://www.gnu.org/software/classpath/license.html.
 * 
 * SPDX-License-Identifier: EPL-2.0 OR GPL-2.0 WITH Classpath-exception-2.0
 * #L%
 */
package info.scce.rig.checks;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.Map;
import java.util.Objects;

import info.scce.rig.mcam.modules.checks.PipelineCheck;
import info.scce.rig.pipeline.Pipeline;
import info.scce.rig.pipeline.Target;

public class UniqueNamesCheck extends PipelineCheck {

	@Override
	public void check(Pipeline model) {
		checkTargetNamesUnique(model);
	}

	private void checkTargetNamesUnique(Pipeline model) {
		Map<String, Long> targetNamesFrequency = model.getTargets().stream().map((t) -> t.getName())
				.filter(Objects::nonNull).collect(groupingBy(e -> e, counting()));
		for (Target t : model.getTargets()) {
			String name = t.getName();
			if (targetNamesFrequency.get(name) > 1)
				addError(t, Target.class.getSimpleName() + " has an ambiguous name \"" + name + "\"");
		}
	}
}
