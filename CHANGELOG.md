## 1.0.2 (TBA)

* Remove underscore from generated filename (see #20)

## 1.0.1 (2021-10-12)

* Remove overly strict validation of script arguments; they are allowed to be configured locally (see #12)
* Add feature flag for stageless pipelines (see #11)
* Hide StageAssignmentStrategy (see #6), complete removal planned for later (see #14)

## 1.0.0 (2021-09-28)

Initial public release of Rig 1.0.0.
