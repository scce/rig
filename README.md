# Rig - Visual CI/CD pipeline authoring

![https://scce.gitlab.io/rig/assets/hero-image.png](https://scce.gitlab.io/rig/assets/hero-image.png)

A visual authoring tool for CI/CD pipeline configurations.

**Code it, rig it, ship it continuosly.**

This tool uses a concept of jobs and targets to describe a CI/CD pipeline
and allows the user to configure them graphically via drag & drop.

## Documentation

Please visit our documentation at [https://scce.gitlab.io/rig/](https://scce.gitlab.io/rig/).

## Download

Downloads of Rig are available at [https://scce.gitlab.io/rig/download](https://scce.gitlab.io/rig/download)

# License

Rig is licensed under the [Eclipse Public License 2.0](/LICENSE.txt)

# Versions & Branching

The `main` branch is the current development branch.
All new features and bug fixes should be targetted at the `main` branch.

Releases are tagged with `v<x>.<y>.<z>` and automatically deployed to `https://ls5download.cs.tu-dortmund.de/rig/release/<version>`.  
Additionally, each major and minor version (1.0, 1.1, 2.0 ...) has their own branch named `release-<x>.<y>`.   
Bug fixes can be backported onto those branches for release into that product line.

Snapshots are only build from the latest commit to the `main` branch and are made available in `https://ls5download.cs.tu-dortmund.de/rig/snapshot/<hash>`

# Binary Build (Docker Based)

- Run  `docker run --entrypoint "/bin/bash" --rm -it -v ${PWD}:/build/scce/rig -v "${HOME}/.m2:/root/.m2" --name cinco registry.gitlab.com/scce/cinco`
- Inside the now open container, run
    - `cd /build/scce/rig`
    - `sh build.sh`
- Wait for the build to finish. The first time around, this can take a significant amount of time to download the dependencies. Subsequent runs are faster.

## Attribution

**Application Icons**

Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com  
License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

**Logo**

Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>  
Flaticon License - Free for personal and commercial purpose with attribution.
